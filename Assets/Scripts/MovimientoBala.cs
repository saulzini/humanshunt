﻿using UnityEngine;
using System.Collections;

public class MovimientoBala : MonoBehaviour {

	public float mover = 0.1f;

	float movimiento = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		this.gameObject.transform.Translate(0, 0, movimiento);
		movimiento = movimiento + mover;

		Destroy (this.gameObject, 1);
	}

	void OnTriggerEnter(Collider other) {
		
		if (other.gameObject.tag == "Lapida") {
			


			Destroy (this.gameObject);

		}
		
	}
}
