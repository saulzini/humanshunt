﻿using UnityEngine;
using System.Collections;

public class Humano : MonoBehaviour {

	// Use this for initialization
	public GameObject personaje;

	public GameObject murcielago;

	GameObject aux;
	public float distanciaParaMoverseHumano=5f;
	public float movimientoHumano = 0.1f;

	public float distanciaSaltito = 1f;

	public float tamanoSalto = 0.1f;


	private bool escapar = false;
	private HUD hudscript;

	void Start () {
	
	}



	// Update is called once per frame
	void FixedUpdate() {


		float distancia = Mathf.Abs(personaje.transform.position.z - transform.position.z);

		//print (distancia);

		if ( distancia < distanciaParaMoverseHumano) {
			escapar =true;

		}
		if (escapar == true) {
			this.gameObject.transform.Translate(0,0, movimientoHumano/10);
		}




	}

	void OnTriggerEnter(Collider other) {

		if (other.gameObject.tag == "Bala") {

			Destroy (other.gameObject);
			Vector3 positionGameObject = this.gameObject.transform.position;
			Destroy (this.gameObject);

			positionGameObject.y = positionGameObject.y + 3;

			aux = Instantiate (murcielago, positionGameObject, transform.rotation) as GameObject;
			Light lightComp = aux.AddComponent<Light> ();
			lightComp.color = Color.blue;
			lightComp.intensity = 50;
			//aux.transform.position = (positionGameObject);

			Destroy (aux, 5);

			HUD.HumanosConvertidos+= 1;
//			print(HUD.HumanosConvertidos);
		}

		if (other.gameObject.tag == "Iglesia") {
			
		
			Destroy (this.gameObject);

		}

	}
}
