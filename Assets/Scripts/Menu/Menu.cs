﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	public CursorMode cursorMode = CursorMode.Auto;
	public Vector2 hotSpot = Vector2.zero;



	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Empezar(){
		Cursor.SetCursor(null, Vector2.zero, cursorMode);
		Application.LoadLevel (4);

	}

	public void menu(){
		Cursor.SetCursor(null, Vector2.zero, cursorMode);
		Application.LoadLevel (0);
		
	}

	public void instrucciones(){
		Cursor.SetCursor(null, Vector2.zero, cursorMode);
		Application.LoadLevel (5);
		
	}



	public void Salir(){

		Application.Quit();
		
	}
}
