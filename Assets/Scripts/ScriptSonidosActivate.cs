﻿using UnityEngine;
using System.Collections;

public class ScriptSonidosActivate : MonoBehaviour {

	// Use this for initialization
	public GameObject personaje;

	// Use this for initialization


	public float disLimite=10f;
	//public AudioClip otherClip;
	private bool empieza=false;

	public AudioClip audioClip;
	private	AudioSource audio;

	void Start() {
		audio = this.gameObject.AddComponent<AudioSource>();
		audio.enabled = true;
		audio.clip = audioClip;
	}
	void Update(){
	//	print (audioClip.name);

			float distancia = Mathf.Abs(personaje.transform.position.z - transform.position.z);
		//print (distancia);
		if ( distancia < disLimite && empieza==false) {
			audio.Play();
			empieza=true;
		}


		if (this.gameObject.tag == "Enemigo") {
			audio.volume = 0.4f;	
		}

	}
}
